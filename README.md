# Statistics

## 1. Sum
**- Find the sum of User Input Numbers**


**Algorithm:**
```
    Step 1: Start
    Step 2: Declare variable numbers and sum
    Step 3: Use loop for assign the values to The sum variable ,
    Step 4: Perform operation (sum= sum+ number)
    Step 5: Print sum variable
    Step 7: Stop
```

**Code**
```
#include <stdio.h>
int main() {
int Sum, i,n,numbers;
printf("\nPlease Enter How many Number you want?\n");
scanf("%d",&n);
printf("\nPlease Enter the elements one by one\n");
for(i=0;i<n;++i)
{
scanf("%d",&numbers);
Sum = Sum +numbers;
}
printf("Sum=%d",Sum);
return 0;
}
```

**Output**
```
Please Enter How many Number you want?
2
Please Enter the elements one by one
1
2
sum=3
```
---
## 2. Average
**- Find The Average Of User Input Numbers**

**Algorithm**
```
 Step 1: Start
    Step 2: Declare variable number and sum
    step 3:assign the size of values into number
    Step 4: Perform operation (Average= sum/number)
    Step 5: Print Average variable
    Step 7: Stop

```

**Code**
```
#include<stdio.h>
void main()
{
int i,n,Sum=0,numbers;
float Average;
printf("\nPlease Enter How many Number you want?\n");
scanf("%d",&n);
printf("\nPlease Enter the elements one by one\n");
for(i=0;i<n;++i)
{
scanf("%d",&numbers);
Sum = Sum +numbers;
}
Average = Sum/n;
printf("\nSum of the %d Numbers = %d",n, Sum);
printf("\nAverage of the %d Numbers = %f",n, Average);
return 0;
}
```

**Output**
```
Please Enter How many Number you want?
2
Please Enter the elements one by one
1
2
Average=1.5
```
---
## 3. Mean
**- Find the mean of user input Numbers**

**Algorithm**
```
Step 1: Start
    Step 2: Declare variable numbers and sum
    Step 3: Use loop for assign the values to The sum variable ,
    Step 4: Perform operation (Mean= sum+ number)
    Step 5: Print mean variable
    Step 7: Stop
```

**Code**
```
#include <stdio.h>
int main() {
float mean;
int Sum, i,n,numbers;
printf("\nPlease Enter How many Number you want?\n");
scanf("%d",&n);
printf("\nPlease Enter the elements one by one\n");
for(i=0;i<n;++i)
{
scanf("%d",&numbers);
Sum = Sum +numbers;
}
float Mean= Sum/n;
printf("Mean=%f",Mean);
return 0;
}
```


**Output**
```
Please Enter How many Number you want?
2
Please Enter the elements one by one
1
2
Mean=1.5
```
---

## 4. Meadian
**-  find the meadian of user input numbers**

**Algorithm**
```
    Start   1:  Start
    Step 2: Declare variable number and  Array
    Step 3: Use loop for assign the values to The Arraya And assign the size of array into number variable
    Step 4: Perform operation (n%2 == 0)
    Step 5: if True Perform Action(median = (a[(n/2)-1]+a[(n/2)])/2.0)
    step 6: else perform Action(median = a[(n/2)];)
    Step 7: Stop
```

**Code**
```
#include<stdio.h>
#define N 5
void main()
{
int i, j, n;
float median, a[N], temp;
printf("Enter number of elements:\n");
scanf("%d", &n);

printf("Input %d values \n", n);
for(i=0; i<n; i++)
{
scanf("%f", &a[i]);
}
temp =0;

for(i=0;i<n-1;i++)
{
for(j=0; j<n-i-1; j++)
{
if(a[j]<a[j+1])
{
temp = a[j];
a[j] = a[j+1];
a[j+1] = temp;
}
}
}
if( n%2 == 0)
median = (a[(n/2)-1]+a[(n/2)])/2.0;
else
median = a[(n/2)];

for(i=0; i<n; i++)
printf("%f\t", a[i]);
printf("\nMedian is %f\n", median);
}
```

**Output**
```
Enter number of elements
3
1
2
3
meadian=3
```
---

### 3. Mode
**- Find The Mode of user Input Numbers**

**Algorithm**
```
    Start   1:  Start
    Step 2: Declare variable number and  Array
    Step 3: Use loop for assign the values to The Arraya And assign the size of array into number variable
    Step 4: use loop to traverse the array for record the value
    Step 5: another loop used for count the reapeted value in the array
    Step 6: Stop
```

**Code**
```
#include <stdio.h>
int main() {
int n,a[0],i;
printf("number of values");
scanf("%d",&n);
printf("enter the value one by one");
for(i=0;i<n;i++){
scanf("%d"&a[i]);
}
printf("Mode = %d ", mode(a,n));
return 0;
}
int mode(int a[],int n) {
int maxValue = 0, maxCount = 0, i, j;
for (i = 0; i < n; ++i) {
int count = 0;
for (j = 0; j < n; ++j) {
if (a[j] == a[i])
++count;
}
if (count > maxCount) {
maxCount = count;
maxValue = a[i];
}
}
return maxValue;
}
```

**Output**
```
number of values
3
enter the value one by one
1
2
2
Mode=3
```
---

## 6. All functions in one program
**- Find the Sum,average,mean,median and mode from the User input Numbers**

**Algorithm**
```
    Step 1: Start
    Step 2: Declare variable number,sum,average,mean,meadian,mode and Array
    Step 3: Use loop for assign the values to The Arraya And assign the size of array into number variable
    Step 4: Perform operation (sum= sum+ number)
    Step 5: Print sum variable
    Step 6: Perform operation (Average= sum/number)
    Step 7: Print sum variable
    Step 8: Perform operation (Average= sum/number)
    Step 9: Print Average variable
    Step 10: Perform operation (Mean= sum+ number)
    Step 11: Print mean variable
    Step 12: Perform operation (n%2 == 0)
    Step 13: if True Perform Action(median = (a[(n/2)-1]+a[(n/2)])/2.0)
    step 14: else perform Action(median = a[(n/2)];)
    Step 15: use loop to traverse the array for record the value
    Step 16: another loop used for count the reapeted value in the array
    Step 17: Stop
```

**Code**
```
#include<stdio.h>

int findSum(int arr[],int size);

float findAverage(int arr[],int size);

float findMean(int arr[],int size);

float findMedian(int arr[], int size);

 int mode(int a[],int n);

 void sort(int a[],int n);


int findSum(int arr[],int size)
{
    int i, sum = 0;
   
    for (i = 0; i < size; i++) {
        sum += arr[i];
    }
    
    return sum ;
}
float findAverage(int arr[],int size)
{
int i;
int value=2;
float sum=0,avg;
   for (i = 0; i<size; i++) {
        sum += arr[i];
    }
    avg=sum/value;
    return avg;
}
float findMean(int arr[],int size)
{
    int i; 
     
     float sum=0,mean; 
         for(i=0;i<size;i++) 
     { 
       sum+=arr[i]; 
           
     } 
    mean=sum/size; 
    return mean;
}
float findMedian(int arr[], int size)
{

}

 void swap(int *p,int *q) {  
     int t;  
   
     t=*p;   
     *p=*q;   
     *q=t;  
  }  

  void sort(int a[],int n) {   
     int i,j,temp;  

     for(i = 0;i < n-1;i++) {  
        for(j = 0;j < n-i-1;j++) {  
           if(a[j] > a[j+1])  
              swap(&a[j],&a[j+1]);  
        }  
     }  }

      int mode(int a[],int n) {  
     int maxValue = 0, maxCount = 0, i, j;  

     for (i = 0; i < n; ++i) {  
         int count = 0;  
            
        for (j = 0; j < n; ++j) {  
           if (a[j] == a[i])  
           ++count;  
        }  
           
        if (count > maxCount) {  
           maxCount = count;  
           maxValue = a[i];  
        }  
    }  
     
     return maxValue;  
  }  

int main()
{
    float avg,mean;
    int sum;
    int i,size;
    printf("Enter The size Of the array =\n");
    scanf("%d",&size);
    printf("enter the numbers one by one =\n");
    int arr[size];
    
    for(i=0;i<size;i++)
    {
        
        scanf("%d",&arr[i]);
        printf("type next number =\n");
    }
    sum = findSum(arr,size);       // name of the array is passed as argument.
    printf("sum of array  = %d\n", sum);


    avg=findAverage(arr,size);
     printf("average  of array  = %f\n", avg);

    mean=findMean(arr,size);
    printf("mean  of array  = %f\n", mean);

      sort(arr,size);  

      size = (size+1) / 2 - 1;       

     printf("Median = %d \n", arr[size]);  



printf("Mode = %d\n ", mode(arr,size));  

    return 0;
}
```

**Output**
```
number of values
3
enter the value one by one
1
Next Number
2
Next Number
2
sum=5
Average=2.5
mean=2.5
meadian=2
Mode=2
```
